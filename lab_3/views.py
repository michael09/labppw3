from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'lab_3/index.html')

def guestBook(request) :
    return render(request, 'lab_3/guestBook.html')

def about(request):
    return render(request, 'lab_3/about.html')