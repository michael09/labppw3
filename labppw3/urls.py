"""labppw3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import re_path, path
from lab_3.views import index as index_lab3
from lab_3.views import about as index_about
from lab_3.views import guestBook as index_guestBook

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r"^$", index_lab3, name='index'),
    re_path(r"^lab-3", index_lab3, name='lab-3'),
    re_path(r"^about", index_about, name='about'),
    re_path(r"guest-book", index_guestBook, name='guest-book')
]